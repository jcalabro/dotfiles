#!/usr/bin/env bash

#
# Install these configuration files on your machine
#

function install_dotfiles {
    echo "Installing"
    pushd "$HOME/misc/dotfiles" > /dev/null

    #
    # Bash scripts
    #
    cp bash/bash_profile $HOME/.bash_profile
    cp bash/bashrc       $HOME/.bashrc
    cp bash/bash_aliases $HOME/.bash_aliases
    cp bash/bash_go      $HOME/.bash_go
    cp bash/bash_work    $HOME/.bash_work

    #
    # Git
    #
    cp git/gitignore           $HOME/.gitignore
    cp git/git-completion.bash $HOME/.git-completion.bash

    #
    # Vim
    #
    cp vim/vimrc       $HOME/.vimrc
    cp vim/vim_plugins $HOME/.vim_plugins
    mkdir -p "$HOME/.config/nvim"
    cp vim/init.vim "$HOME/.config/nvim"

    popd > /dev/null

    pushd $HOME > /dev/null
    source .bash_profile
    popd > /dev/null
}

echo -n "Are you sure (y/N)? "
read answer
if [ "$answer" != "${answer#[Yy]}" ] ;then
    install_dotfiles
    source "$HOME/.bash_profile"
    echo "Done"
else
    echo "Exiting"
    return
fi
